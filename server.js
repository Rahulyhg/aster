const restify = require("restify");
const aster = require("./aster.js");

let server = restify.createServer({
    name: "aster"
});

server.use(restify.queryParser());
server.use(restify.CORS());

server.get("/planets", function(req, res, next)
{
    let planets = [];
    let date = req.query.date ? new Date(req.query.date) : new Date();
    for (var i = 0; i < 11; i++)
    {
        let planet = aster.getPlanet(i, date);
        planets.push(planet);
    }
    let aspects = aster.getAspects(planets);
    res.send({ planets: planets, aspects: aspects });
    return next();
});

server.get("/planet/:name", function(req, res, next)
{
    let date = req.query.date ? new Date(req.query.date) : new Date();
    let planet = aster.getPlanet(req.params.name, date);
    res.send(planet);
    return next();
});

server.get("/houses", function(req, res, next)
{
    let date = req.query.date ? new Date(req.query.date) : new Date();

    // seattle: 47.6062° N, 122.3321° W
    let lat = parseFloat(req.query.lat) || 47.6062;
    let long = parseFloat(req.query.long) || -122.3321;

    let houses = aster.getHouses({ lat, long }, date);
    res.send({
        asc: houses.ascendant,
        mc: houses.mc,
        houses: houses.house
    });
    return next();
});

const port = 3000;
console.log("[restify] aster listening on port " + port);
server.listen(port);
