const swisseph = require("swisseph");

const aster = {};

aster.planets = [
    { id: swisseph.SE_SUN, name: "Sun", unicode: "☉" },
    { id: swisseph.SE_MOON, name: "Moon", unicode: "☾" },
    { id: swisseph.SE_MERCURY, name: "Mercury", unicode: "☿" },
    { id: swisseph.SE_VENUS, name: "Venus", unicode: "♀" },
    { id: swisseph.SE_EARTH, name: "Earth", unicode: "♁" },
    { id: swisseph.SE_MARS, name: "Mars", unicode: "♂" },
    { id: swisseph.SE_JUPITER, name: "Jupiter", unicode: "♃" },
    { id: swisseph.SE_SATURN, name: "Saturn", unicode: "♄" },
    { id: swisseph.SE_URANUS, name: "Uranus", unicode: "♅" },
    { id: swisseph.SE_NEPTUNE, name: "Neptune", unicode: "♆" },
    { id: swisseph.SE_PLUTO, name: "Pluto", unicode: "♇" },
    { id: swisseph.SE_MEAN_NODE, name: "Mean Node", unicode: "☊" },
    { id: swisseph.SE_TRUE_NODE, name: "True Node", unicode: "☊" },
    { id: swisseph.SE_CHIRON, name: "Chiron" },
    { id: swisseph.SE_PHOLUS, name: "Pholus" },
    { id: swisseph.SE_CERES, name: "Ceres" },
    { id: swisseph.SE_PALLAS, name: "Pallas" },
    { id: swisseph.SE_JUNO, name: "Juno" },
    { id: swisseph.SE_VESTA, name: "Vesta" },
];

aster.signs = [
    { id: 1, name: "Aries", unicode: "♈", element: "fire", mode: "cardinal" },
    { id: 2, name: "Taurus", unicode: "♉", element: "earth", mode: "fixed" },
    { id: 3, name: "Gemini", unicode: "♊", element: "air", mode: "mutable" },
    { id: 4, name: "Cancer", unicode: "♋", element: "water", mode: "cardinal" },
    { id: 5, name: "Leo", unicode: "♌", element: "fire", mode: "fixed" },
    { id: 6, name: "Virgo", unicode: "♍", element: "earth", mode: "mutable" },
    { id: 7, name: "Libra", unicode: "♎", element: "air", mode: "cardinal" },
    { id: 8, name: "Scorpio", unicode: "♏", element: "water", mode: "fixed" },
    { id: 9, name: "Sagittarius", unicode: "♐", element: "fire", mode: "mutable" },
    { id: 10, name: "Capricorn", unicode: "♑", element: "earth", mode: "cardinal" },
    { id: 11, name: "Aquarius", unicode: "♒", element: "air", mode: "fixed" },
    { id: 12, name: "Pisces", unicode: "♓", element: "water", mode: "mutable" },
];

aster.aspects = [
    { id: 1, name: "conjunct", angle: 0, maxOrb: 8 },
    { id: 2, name: "opposite", angle: 180, maxOrb: 5 },
    { id: 3, name: "square", angle: 90, maxOrb: 5 },
    { id: 4, name: "trine", angle: 120, maxOrb: 5 },
    { id: 5, name: "sextile", angle: 60, maxOrb: 3 }
];

aster.getPlanet = function(idOrName, date = null, minimal = false)
{

    const id = parseInt(idOrName);
    const isId = !isNaN(id)
    let planets = this.planets.filter(planet => isId ?
        planet.id == id :
        planet.name.toUpperCase() == idOrName.toUpperCase());

    if (planets.length != 1) return {
        error: true,
        message: isId ?
            `Planet id ${id} not found.` :
            `Planet '${idOrName}' not found.`};

    let planet = minimal ? { id: planets[0].id } : planets[0];
    if (date != null)
    {
        planet.position = getPosition(planet.id, date, minimal);
    }

    return planet;

}

aster.getHouses = function(location, date = new Date(), minimal = false)
{

    // swe_houses(julday, geolat, geolong, hsys)

    // latitude: northern is positive, southern is negative
    // longitude: eastern is positive, western is negative

    // hsys = P - Placidus
    //        K - Koch
    //        O - Porphyrius
    //        R - Regiomontanus
    //        C - Campanus
    //        A/E - Equal (cusp 1 is Ascendant)
    //        W - Whole sign

    let julday = getJulDate(date);
    let houses = swisseph.swe_houses(julday, location.lat, location.long, 'P');
    return houses;

};

aster.getAspects = function(planets, planets2, minimal = false)
{

    let singleChart = !planets2;
    if (singleChart) planets2 = planets;
    let aspects = [];

    for (var i = 0; i < planets.length; i++)
    {
        for (var j = singleChart ? i + 1 : 0; j < planets2.length; j++)
        {
            for (var a = 0; a < this.aspects.length; a++)
            {
                let position1 = planets[i].position.longitude;
                let position2 = planets2[j].position.longitude;
                let diff = Math.abs(position1 - position2);
                let aspect = this.aspects[a];
                if (diff >= aspect.angle && diff <= aspect.angle + aspect.maxOrb)
                {
                    aspects.push({
                        planet1: minimal ? planets[i].id : planets[i],
                        planet2: minimal ? planets2[j].id : planets2[j],
                        aspect: minimal ? aspect.id : aspect,
                        orb: diff - aspect.angle });
                }
            }
        }
    }

    return aspects;

}

getPosition = function(planetId, date = new Date(), minimal = false)
{

    let julday = getJulDate(date);
    let flag = swisseph.SEFLG_SPEED | swisseph.SEFLG_SWIEPH;

    let body = swisseph.swe_calc_ut(julday, planetId, flag);
    if (minimal) return body.longitude;

    let position = getPositionFromDegree(body.longitude);
    position.speed = body.longitudeSpeed;
    return position;

};

getJulDate = function(date)
{
    let julday = swisseph.swe_julday(date.getUTCFullYear(), date.getUTCMonth() + 1, date.getUTCDate(), date.getUTCHours(), swisseph.SE_GREG_CAL);
    julday += date.getUTCMinutes() / (24 * 60) + date.getUTCSeconds() / (24 * 60 * 60);

    //console.log(`year: ${date.getUTCFullYear()}, month: ${date.getUTCMonth() + 1}, day: ${date.getUTCDate()}, hour: ${date.getUTCHours()}`);
    //console.log(`julian date: ${julday}`);

    return julday;
};

getPositionFromDegree = function(degree)
{
    let h = degree / 30;
    let d = degree - Math.floor(h) * 30;
    let m = (d - Math.floor(d)) * 60;
    let s = (m - Math.floor(m)) * 60;

    return {
        longitude: degree,
        sign: aster.signs[Math.floor(h)],
        degree: Math.floor(d),
        minute: Math.floor(m),
        second: Math.floor(s)
    }
}

module.exports = aster;