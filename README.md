# Aster
*A JavaScript astrology API.*

## Usage (TODO)
- `getPlanet`: gets a single planet's information, including position if a date is specified.
- `getPlanets`: gets multiple planets' information.
- `getHouses`: gets ascendant, midheaven, and house cusp positions.